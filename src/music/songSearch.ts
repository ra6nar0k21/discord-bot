import { getInvidiousPlaylist, getInvidiousVideo, InvidiousVideo, searchInvidious } from '@/invidious/invidious';
import { isUrl } from '@/utils';
import { TrackData } from './queue';

class SongSearch {
  /**
   * Prioritises Playlists
   * @param titleOrUrl Url or Searchquery
   * @returns A list of videos
   */
  public static async query(titleOrUrl: string): Promise<TrackData[]> {
    const metadataFromVideo = (video: InvidiousVideo): TrackData => ({
      title: video.title,
      thumbnail: video.thumbnail,
      duration: video.lengthSeconds,
      source: 'invidious',
      src: video.videoId,
    });

    if (isUrl(titleOrUrl)) {
      let result: Array<TrackData> = [];

      if (titleOrUrl.indexOf('list=') !== -1) {
        const playlist = await getInvidiousPlaylist(titleOrUrl);

        if (playlist !== undefined) {
          result.push(...playlist.videos.filter(video => !video.liveNow).map(video => metadataFromVideo(video)));
        }
      }

      if (titleOrUrl.indexOf('v=') !== -1) {
        try {
          const video = await getInvidiousVideo(`${titleOrUrl}`);
          if (video !== undefined) {
            // video
            const metadata = metadataFromVideo(video);
            result = result.filter(data => {
              if (data.src === metadata.src) {
                return false;
              }
              return true;
            });
            result.unshift(metadata);
            // console.log(metadata);
          }
        } catch (err) {
          return Promise.reject(err);
        }
      }
      return result;
    } else {
      const search = await searchInvidious(titleOrUrl, 1);
      return [metadataFromVideo(search[0])];
    }
  }
}

export default SongSearch;
