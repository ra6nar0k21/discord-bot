import audioFromInvidious from '@/invidious/invidious';
import { Awaited } from '@/typeUtils';
import { AudioPlayer, AudioPlayerStatus, AudioResource, createAudioPlayer, createAudioResource, NoSubscriberBehavior, VoiceConnection } from '@discordjs/voice';
import { randomInt } from 'crypto';
import { Snowflake } from 'discord-api-types';
import { Guild, User } from 'discord.js';
import { TypedEmitter } from 'tiny-typed-emitter';

// TODO: Fix stop to soon

type TrackData = {
  title: string;
  thumbnail: string;
  duration: number;
  source: 'invidious';
  src: string;

  requestedBy?: User;
}

/**
 * The type for the looping option
 */
enum LoopMode {
  Song,
  Queue,
  Disabled,
}

/**
 * The events for the queue
 */
type QueueEvents = {
  tracksAdded: (tracks: TrackData[]) => Awaited<void>;
  trackRemoved: (track?: TrackData) => Awaited<void>;

  trackPaused: () => Awaited<void>;
  trackUnpaused: () => Awaited<void>;
  trackStopped: () => Awaited<void>;

  trackPlaying: (track: TrackData) => Awaited<void>;
  trackSkipped: (tracks: Array<TrackData>) => Awaited<void>;
  //queueChanged: () => Awaited<void>;

  queueCleared: () => Awaited<void>;

  loopTypeChanged: (type: LoopMode) => Awaited<void>;

  playerReady: () => Awaited<void>;
  playerError: (error: Error) => Awaited<void>;
  resourceCreationError: (error: Error, track: TrackData) => Awaited<void>;
};

class Queue extends TypedEmitter<QueueEvents> {
  private readonly musicPlayer: AudioPlayer;

  private readonly guildId: Snowflake;

  private tracks: Array<TrackData> = [];
  private loopMode: LoopMode = LoopMode.Disabled;
  private shuffle = false;

  private voiceConnectionReady = false;

  private currentTrack?: TrackData;

  private closeCurrentTrack?: () => void;

  constructor(
    guild: Guild,
  ) {
    super();

    this.guildId = guild.id;

    this.musicPlayer = createAudioPlayer({
      behaviors: {
        noSubscriber: NoSubscriberBehavior.Pause,
      },
    });

    this.musicPlayer.on('error', error => {
      console.log('Error on music Player', error);

      this.emit('playerError', error);
    });

    this.musicPlayer.on(AudioPlayerStatus.Idle, () => {
      // Play next track
      if (this.voiceConnectionReady) {
        this.playNextTrack();
      }
    });
  }

  /**
   * @param track The track to be used
   * @returns An audio resource from the track
   */
  private async createAudioResourceFromTrack(track: TrackData): Promise<AudioResource> {
    this.closeCurrentTrack?.call(undefined);

    if (track.source === 'invidious') {
      const data = await audioFromInvidious(track.src);
      const resource = createAudioResource(data);
      this.closeCurrentTrack = () => data.destroy();
      return resource;
    }

    return Promise.reject(new Error(''));
  }

  /**
   * The current playing track. If none, undefined
   */
  public get currentPlayingTrack(): TrackData | undefined {
    return this.currentTrack;
  }

  /**
   * Allows the player to be playing songs
   */
  public setReady(connection: VoiceConnection): void {
    this.voiceConnectionReady = true;
    connection.subscribe(this.musicPlayer);

    this.emit('playerReady');
  }

  /**
   * The size of the queue
   */
  public get length(): number {
    return this.tracks.length;
  }

  /**
   * The current loop mode
   */
  public get currentLoopMode(): LoopMode {
    return this.loopMode;
  }

  /**
   * Adds a track to the end of the queue
   */
  public enqueueBack(tracks: TrackData[], requestedBy?: User): void {
    for (const track of tracks) {
      if (track.requestedBy === undefined) { track.requestedBy = requestedBy; }
    }
    this.tracks.push(...tracks);

    this.emit('tracksAdded', tracks);

    if (this.currentTrack === undefined) {
      this.playNextTrack();
    }
  }

  /**
   * Adds a track to the front of the queue
   */
  public enqueueFront(tracks: TrackData[], requestedBy?: User): void {
    for (const track of tracks) {
      if (track.requestedBy === undefined) { track.requestedBy = requestedBy; }
    }
    this.tracks.unshift(...tracks.reverse());

    this.emit('tracksAdded', tracks);

    if (this.currentTrack === undefined) {
      this.playNextTrack();
    }
  }

  /**
   * Removes a track at index
   */
  public removeTrack(index: number): void {
    if (index > this.tracks.length || index < 0) {
      this.emit('trackRemoved');
      return;
    }
    const track = this.tracks.splice(index, 1)[0];
    this.emit('trackRemoved', track);
  }

  /**
   * Plays a track directly
   */
  public playTrack(track: TrackData, requestedBy?: User): void {
    if (track.requestedBy === undefined) { track.requestedBy = requestedBy; }
    this.createAudioResourceFromTrack(track)
      .then(resource => {
        this.musicPlayer.play(resource);

        this.emit('trackPlaying', track);
      })
      .catch(err => {
        this.emit('resourceCreationError', err, track);
        console.error(err);
      });
  }

  /**
   * Plays the next track
   */
  public playNextTrack(): void {
    if (this.shuffle) {
      const index = randomInt(this.length + 1);
      const track = this.spliceTrack(index);
      if (track !== undefined) {
        this.playTrack(track);
      }
    } else {
      if (this.loopMode === LoopMode.Song) {
        if (this.currentTrack !== undefined) {
          this.playTrack(this.currentTrack);
          return;
        }
      } else if (this.loopMode === LoopMode.Queue) {
        if (this.currentTrack !== undefined) {
          this.enqueueBack([this.currentTrack]);
        }
      }
      this.currentTrack = this.spliceTrack(0);
      if (this.currentTrack !== undefined) {
        this.playTrack(this.currentTrack);
      }
    }
  }

  /**
   * Returns the splice and removes it from the queue
   */
  private spliceTrack(index: number): TrackData | undefined {
    if (index < 0) {
      return undefined;
    }
    if (index > this.length) {
      return undefined;
    }
    return this.tracks.splice(index, 1)[0];
  }

  /**
   * Returns the track from a given index
   */
  public getTrackAt(index: number): TrackData | undefined {
    if (index < 0) {
      return undefined;
    }
    if (index > this.length) {
      return undefined;
    }
    return this.tracks[index];
  }

  /**
   * Calculates the estimated time until a track will be played
   */
  public calulateLengthUntilTrack(index: number): number {
    if (index < 0) {
      return 0;
    }
    index = Math.min(index, this.length);
    let duration = 0;
    for (let i = 0; i < index; i++) {
      duration += this.tracks[i].duration;
    }

    if (this.currentTrack !== undefined) {
      duration += this.currentTrack.duration;
    }

    return duration;
  }

  /**
   * Skips tracks
   * @param amount The amount to be skipped
   */
  public skipTrack(amount = 1): number {
    if (this.length === 0) {
      return 0;
    }
    amount = Math.min(amount - 1, this.length);
    this.tracks.splice(0, amount);
    this.stopTrack();
    return amount + 1;
  }

  /**
   * Pauses the music player
   */
  public pauseTrack(): void {
    this.musicPlayer.pause();

    this.emit('trackPaused');
  }

  /**
   * Stops the playback of the current song and destroys the resource
   */
  public stopTrack(): void {
    this.musicPlayer.stop();

    this.emit('trackStopped');
  }

  /**
   * Unpauses the music player
   */
  public unpauseTrack(): void {
    this.musicPlayer.unpause();

    this.emit('trackUnpaused');
  }

  /**
   * Changes the loop type
   */
  public setLoopMode(loopType: LoopMode): void {
    this.loopMode = loopType;

    this.emit('loopTypeChanged', loopType);
  }

  /**
   * Sets a looptype from string
   */
  public setLoopModeFromString(loopType: string): void {
    const keys = Object.keys(LoopMode);
    for (let i = 0; i < keys.length; i++) {
      const loop = keys[i];
      if (isNaN(Number(loop))) {
        if (loop.toLowerCase() === loopType.toLowerCase()) {
          this.setLoopMode(i - keys.length / 2);
        }
      }
    }
  }

  /**
   * Clears the queue
   */
  public clearQueue(): void {
    this.tracks = [];

    this.emit('queueCleared');
  }
}

export default Queue;

export { LoopMode };
export { TrackData };
