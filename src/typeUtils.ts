type Awaited<T> = T | Promise<T>;

export { Awaited };
