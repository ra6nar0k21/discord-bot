import Command from '@/commands/command';
import CommandHandler from '@/commands/commandHandler';
import { Client, Intents } from 'discord.js';
import MusicHandler from '@/music/musicHandler';

class DiscordBot {
  public readonly client: Client;
  public readonly commandHandler: CommandHandler;
  public readonly musicHandler: MusicHandler;

  private readonly token: string;

  constructor(token: string, prefix: string) {
    this.token = token;

    this.client = new Client(
      {
        intents: [
          Intents.FLAGS.GUILDS,
          /** Handle guild messages */
          Intents.FLAGS.GUILD_MESSAGES,
          /** Handle direct messages */
          Intents.FLAGS.DIRECT_MESSAGES,
          //Intents.FLAGS.GUILD_MESSAGE_TYPING,
          /** Handle guild message reactions */
          Intents.FLAGS.GUILD_MESSAGE_REACTIONS,
          /** Handle direct message reactions */
          Intents.FLAGS.DIRECT_MESSAGE_REACTIONS,
          /** Handle voice states */
          Intents.FLAGS.GUILD_VOICE_STATES,
        ],
      },
    );

    this.commandHandler = new CommandHandler();
    this.musicHandler = new MusicHandler(this);

    this.client.once('ready', () => {
      console.log('Ready!');
    });

    this.client.on('messageCreate', async (msg) => {
      if (msg.author.bot) {
        return;
      }

      const content = msg.content;

      if (content.startsWith(prefix)) {
        const commandRaw = content.substr(1);
        this.commandHandler.parse(msg, commandRaw);
      }
    });
  }

  public login(): void {
    this.client.login(this.token);
  }

  public addCommand(command: Command): void {
    this.commandHandler.addCommand(command);
  }
}

export default DiscordBot;
