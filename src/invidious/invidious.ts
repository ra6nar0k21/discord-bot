import { isUrl } from '@/utils';
import { spawn } from 'child_process';
import fetch from 'cross-fetch';
import * as fs from 'fs';

import pathToFfmpeg from 'ffmpeg-static';
import internal from 'stream';

const invidiousInstance = 'https://invidio.xamh.de';

/**
 * This is the main Video type
 */
type InvidiousVideo = {
  title: string;
  videoId: string;
  author: string;

  lengthSeconds: number;
  liveNow: boolean;

  thumbnail: string;
};

type InvidiousPlaylist = {
  title: string;
  playlistId: string;
  author: string;

  videos: Array<InvidiousVideo>;
}

const extractVideoIdRegEx = /(?<=v=)([A-Za-z0-9_-]{11})/;
const extractPlaylistIdRegEx = /(?<=list=)([A-Za-z0-9_-]{34})/;

/**
 * Extracts the id of a url or returns directly the id if it is no url
 */
const extractId = (idOrUrl: string, type: 'video' | 'playlist' = 'video'): string => {
  let id = idOrUrl;
  if (isUrl(idOrUrl)) {
    const result = type === 'video' ? extractVideoIdRegEx.exec(idOrUrl) : extractPlaylistIdRegEx.exec(idOrUrl);
    if (result === null) {
      return '';
    }
    id = result[0];
    if (id === undefined) {
      return '';
    }
  }
  return id;
};

const videoFields = 'title,videoId,author,lengthSeconds,liveNow,videoThumbnails(url)';
const playlistFields = `title,playlistId,author,videos(${videoFields})`;

/**
 * Gets the invidous stream from an url or id
 */
const resolveInvidiousVideoStream = async (video: string): Promise<string> => {
  let link = '';

  const url = `${invidiousInstance}/api/v1/videos/${extractId(video)}?fields=formatStreams`;

  await fetch(url, {
    method: 'GET',
  }).then(result => result.json())
    .then((jsonResult) => {
      const streams: Array<any> = jsonResult.formatStreams;

      if (streams.length !== 0) {
        link = streams.pop().url;
      }
    });

  return link;
};

/**
 * This searches the invidious api for a specific playlist by id or url
 */
const getInvidiousPlaylist = async (idOrUrl: string): Promise<InvidiousPlaylist | undefined> => {
  let playlist: InvidiousPlaylist | undefined;

  const id = extractId(idOrUrl, 'playlist');
  const url = `${invidiousInstance}/api/v1/playlists/${id}?fields=${playlistFields}`;
  await fetch(url, {
    method: 'GET',
  }).then(result => result.json())
    .then(jsonResult => {
      playlist = {
        author: jsonResult.author,
        title: jsonResult.title,
        playlistId: jsonResult.playlistId,
        videos: [],
      };
      for (let i = 0; i < jsonResult.videos.length; i++) {
        const video = jsonResult.videos[i];
        playlist?.videos.push({
          title: video.title,
          videoId: video.videoId,
          author: video.author,
          lengthSeconds: video.lengthSeconds,
          liveNow: video.liveNow ?? false,
          thumbnail: video.videoThumbnails[0].url,
        });
      }
    });

  return playlist;
};

/**
 * This searches the invidious api for a specific video by id or url
 * @param id The id of the video
 * @returns The video if found, undefined otherwise
 */
const getInvidiousVideo = async (idOrUrl: string): Promise<InvidiousVideo | undefined> => {
  const id = extractId(idOrUrl);

  const url = `${invidiousInstance}/api/v1/videos/${id}?fields=${videoFields}`;
  let video: InvidiousVideo | undefined;
  await fetch(url, {
    method: 'GET',
  }).then(result => result.json())
    .then(jsonResult => {
      // console.log(jsonResult);

      if (jsonResult !== undefined) {
        video = {
          title: jsonResult.title,
          videoId: jsonResult.videoId,
          author: jsonResult.author,
          lengthSeconds: jsonResult.lengthSeconds,
          liveNow: jsonResult.liveNow,
          thumbnail: jsonResult.videoThumbnails[0].url,
        };
      }
    });
  return video;
};

/**
 * Searches the invidious api for videos by title
 * @param query The query to search for
 * @param amount The amount of elements to be returned
 */
const searchInvidious = async (query: string, amount = 2): Promise<Array<InvidiousVideo>> => {
  const url = `${invidiousInstance}/api/v1/search?q=${encodeURI(query)}&fields=${videoFields}`;
  const data: Array<InvidiousVideo> = [];

  await fetch(url, {
    method: 'GET',
    //body: data
  }).then(result => result.json())
    .then((jsonResult: Array<any>) => {
      const resultLength = jsonResult.length;
      amount = Math.min(resultLength, amount);

      for (let i = 0; i < amount; i++) {
        const elem = jsonResult[i];

        data.push({
          title: elem.title,
          videoId: elem.videoId,
          author: elem.author,
          lengthSeconds: elem.lengthSeconds,
          liveNow: elem.liveNow,
          thumbnail: elem.videoThumbnails[0].url,
        });
      }
    });

  return data;
};

const bufferFile = 'buffer.wav';

/**
 * Creates an audio stream from an url
 */
const audioFromInvidious = async (link: string): Promise<internal.Readable> => {
  // Resolve link
  const url = await resolveInvidiousVideoStream(link);

  const args = [
    '-i', url, // Input
    '-f', 'wav', // format
    '-vn', // No video
    '-ar', '44100', // Sampling rate
    '-ac', '2', // Stereo
    'pipe:1',
    '-err_detect', 'ignore_err', // Ignore errors
    '-reconnect', '1', // Reconnect if failed
    '-reconnect_streamed', '1',
    '-reconnect_delay_max', '5',
  ];
  const ffmpeg = spawn(pathToFfmpeg, args);
  const writeStream = await ffmpeg.stdout.pipe(fs.createWriteStream(bufferFile, { flags: 'w' }));

  await new Promise(resolve => setTimeout(resolve, 5000));
  const readStream = fs.createReadStream(bufferFile);

  readStream.on('close', () => {
    writeStream.destroy();
  });

  readStream.on('end', () => {
    writeStream.end();

    fs.rmSync(bufferFile);
  });
  readStream.on('error', err => console.error(err));
  return readStream;
};

export { getInvidiousVideo, searchInvidious, getInvidiousPlaylist };
export { InvidiousVideo, InvidiousPlaylist };
export default audioFromInvidious;
