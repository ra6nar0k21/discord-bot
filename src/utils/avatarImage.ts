import axios from 'axios';
import sharp from 'sharp';

const avatarUrlToPng = async (url: string | null | undefined) => {
  if (url === null || url === undefined) {
    return await sharp((await axios({ url: 'https://img.welt.de/img/regionales/berlin/mobile101125938/3922508567-ci102l-w1024/inder-DW-Berlin-Altlandsberg-jpg.jpg', responseType: 'arraybuffer' })).data as Buffer)
      .toFormat('png')
      .toBuffer();
  }
  return await sharp((await axios({ url: url, responseType: 'arraybuffer' })).data as Buffer)
    .toFormat('png')
    .toBuffer();
};

const ImageUtilsImpl = {
  avatarUrlToPng,
};

export { ImageUtilsImpl as ImageUtils };
