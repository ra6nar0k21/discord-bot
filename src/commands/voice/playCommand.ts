import Command, { CommandCategory, CommandParameter, Parameters } from '@/commands/command';
import MusicHandler from '@/music/musicHandler';
import SongSearch from '@/music/songSearch';
import { getVoiceConnection } from '@discordjs/voice';
import { Message, MessageEmbed } from 'discord.js';
import { prefix } from '@/config.json';
import { durationToString } from '@/utils';

class PlayCommand implements Command {
  constructor(private handler: MusicHandler) { }

  name = 'play';

  description = `Dieser Befehl fügt einen Song zu der Queue hinzu. Ist der Bot nicht verbunden, verbindet er sich. Genauere infos mit ${prefix}help play`;

  aliases: string[] = ['p'];

  parameters: CommandParameter[] = [{
    name: 'query',
    description: 'Dieser Parameter wird für die Suche verwendet. Er kann eine Playlist, ein Video oder ein Suchbegriff sein.',
    required: false,
  }];

  category = CommandCategory.Voice;

  execute(msg: Message<boolean>, parameters: Parameters): void | Promise<void> {
    if (!msg.member?.voice.channel) {
      //TODO: No channel
      return;
    }

    if (!msg.guild) {
      return;
    }
    const guild = msg.guild;
    const queue = this.handler.getQueue(guild);

    if (this.parameters.length === 0) {
      queue.unpauseTrack();
      return;
    }

    const query = parameters.join(' ');

    const addSongs = () => {
      SongSearch.query(query).then(result => {
        queue.enqueueBack(result, msg.author);

        const track = result[0];
        const embed = new MessageEmbed();
        embed.setTitle(track.title);
        embed.addField('Dauer: ', durationToString(track.duration));
        embed.addField('Hinzugefügt von:', `<@${track.requestedBy?.id}>`);
        if (result.length > 1) {
          embed.setFooter(`Und ${result.length - 1} weitere ${result.length > 1 ? 'Songs' : 'Song'}`);
        }
        // embed.setThumbnail(track.thumbnail);

        msg.reply({ embeds: [embed] });

        msg.reply(`Füge ${result.length} ${result.length > 1 ? 'Songs' : 'Song'} hinzu!`);
      }).catch(err => console.log(err));
    };

    const connection = getVoiceConnection(guild.id);

    if (connection === undefined) {
      // Join if not connected
      this.handler.join(msg.member.voice.channel, () => {
        addSongs();
      });
    } else {
      addSongs();
    }

    // TODO: play spotify songs
    // TODO: play soundcloud songs
  }
}

export default PlayCommand;
