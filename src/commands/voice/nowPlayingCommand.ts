import { Message, MessageEmbed } from 'discord.js';
import Command, { CommandCategory, Parameters } from '@/commands/command';
import MusicHandler from '@/music/musicHandler';
import { durationToString } from '@/utils';

// TODO: Add playback bar
class NowPlayingCommand implements Command {
  constructor(private musicHandler: MusicHandler) { }

  name = 'nowPlaying';

  description = 'Dieser Befehl zeigt den Song an, welcher gerade läuft.';

  aliases: string[] = ['np'];

  category = CommandCategory.Voice;

  execute(msg: Message<boolean>, parameters: Parameters): void | Promise<void> {
    if (!msg.member?.voice.channel) {
      //TODO: No channel
      return;
    }

    if (!msg.guild) {
      return;
    }
    const guild = msg.guild;
    const audioQueue = this.musicHandler.getQueue(guild);
    const track = audioQueue.currentPlayingTrack;

    if (track === undefined) {
      msg.reply('Es wird gerade kein Song gespielt!');
      return;
    }

    const embed = new MessageEmbed();
    embed.setTitle(track.title);
    embed.addField('Dauer: ', durationToString(track.duration));
    embed.addField('Hinzugefügt von:', `<@${track.requestedBy?.id}>`);
    embed.setThumbnail(track.thumbnail);

    msg.reply({ embeds: [embed] });
  }
}

export default NowPlayingCommand;
