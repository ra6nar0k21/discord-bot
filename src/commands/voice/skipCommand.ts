import Command, { CommandCategory, CommandParameter, Parameters } from '@/commands/command';
import { prefix } from '@/config.json';
import MusicHandler from '@/music/musicHandler';
import { Message } from 'discord.js';

class SkipCommand implements Command {
  constructor(private musicHandler: MusicHandler) { }

  name = 'skip';

  description = `Dieser Befehl überspringt den Song, welcher gerade spielt. Genauere infos mit ${prefix}help play`;

  aliases: string[] = [];

  category = CommandCategory.Voice;

  parameters: CommandParameter[] = [{
    name: 'count',
    description: 'Die Anzahl der zu überspringenden Songs. Standardmäßig 1.',
    required: false,
  }];

  execute(msg: Message<boolean>, parameters: Parameters): void | Promise<void> {
    if (!msg.member?.voice.channel) {
      //TODO: No channel
      return;
    }

    if (!msg.guild) {
      return;
    }
    const guild = msg.guild;
    const queue = this.musicHandler.getQueue(guild);

    let count = 1;

    if (parameters.length !== 0) {
      const parsedCount = Number(parameters[0]);
      if (isNaN(parsedCount)) {
        msg.reply(`${parameters[0]} ist keine Zahl!`);
        return;
      }
      count = parsedCount;
    }

    const amountSkipped = queue.skipTrack(count);

    msg.reply(`${amountSkipped} ${amountSkipped > 1 ? 'Songs' : 'Song'} wurden übersprungen!`);
  }
}

export default SkipCommand;
