import Command, { CommandCategory, CommandParameter, Parameters } from '@/commands/command';
import { prefix } from '@/config.json';
import MusicHandler from '@/music/musicHandler';
import { Message } from 'discord.js';

class RemoveTrackCommand implements Command {
  constructor(private musicHandler: MusicHandler) { }

  name = 'remove';

  description = `Dieser Befehl entfernt einen Song aus der Queue. Genauere infos mit ${prefix}help remove`;

  aliases: string[] = [];

  category = CommandCategory.Voice;

  parameters: CommandParameter[] = [{
    name: 'index',
    description: 'Die Position des Songs.',
    required: true,
  }];

  execute(msg: Message<boolean>, parameters: Parameters): void | Promise<void> {
    if (!msg.member?.voice.channel) {
      //TODO: No channel
      return;
    }

    if (!msg.guild) {
      return;
    }
    const guild = msg.guild;
    const queue = this.musicHandler.getQueue(guild);

    queue.once('trackRemoved', track => {
      if (track === undefined) {
        msg.reply('Konnte den Song nicht entfernen!');
      } else {
        msg.reply(`${track.title} wurde entfernt!`);
      }
    });

    const index = parameters[0];
    if (index === undefined) {
      msg.reply('Benötige Index argument');
      return;
    }

    if (isNaN(Number(index))) {
      msg.reply('Index muss eine Zahl sein!');
    }

    queue.removeTrack(Number(index));
  }
}

export default RemoveTrackCommand;
