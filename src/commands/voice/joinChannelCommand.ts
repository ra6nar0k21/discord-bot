import Command, { CommandCategory, Parameters } from '@/commands/command';
import MusicHandler from '@/music/musicHandler';
import { Message } from 'discord.js';
import { errors } from 'errors';

class JoinChannelCommand implements Command {
  constructor(private musicHandler: MusicHandler) { }

  name = 'join';

  description = 'Mit diesem Befehl tritt der Bot in den Channel bei, in welchem du bist.';

  aliases: string[] = [];

  category: CommandCategory = CommandCategory.Voice;

  async execute(msg: Message<boolean>, parameters: Parameters): Promise<void> {
    const voiceChannel = msg.member?.voice.channel;

    if (!voiceChannel) {
      msg.reply(errors.voice.noVoiceChannel);
      return;
    }

    this.musicHandler.join(voiceChannel);

    // Join message
    msg.reply(`Trete dem Channel ${voiceChannel.name} bei!`);
  }
}

export default JoinChannelCommand;
