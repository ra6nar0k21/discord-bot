import Command, { CommandCategory, CommandParameter, Parameters } from '@/commands/command';
import MusicHandler from '@/music/musicHandler';
import { Message } from 'discord.js';

class PauseCommand implements Command {
  constructor(private handler: MusicHandler) { }

  name = 'pause';

  description = `Dieser Befehl pausiert den spielenden Song`;

  aliases: string[] = [];

  parameters: CommandParameter[] = [];

  category = CommandCategory.Voice;

  execute(msg: Message<boolean>, parameters: Parameters): void | Promise<void> {
    if (!msg.member?.voice.channel) {
      //TODO: No channel
      return;
    }

    if (!msg.guild) {
      return;
    }
    const guild = msg.guild;
    const queue = this.handler.getQueue(guild);

    queue.pauseTrack();
  }
}

export default PauseCommand;
