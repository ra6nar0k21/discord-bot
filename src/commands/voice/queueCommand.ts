import Command, { CommandCategory, Parameters } from '@/commands/command';
import MusicHandler from '@/music/musicHandler';
import { durationToString } from '@/utils';
import { Client, Message, MessageEmbed, MessageReaction, PartialMessageReaction, PartialUser, User } from 'discord.js';

class DisplayQueueCommand implements Command {
  constructor(private musicHandler: MusicHandler, private readonly client: Client) { }

  name = 'queue';

  description = 'Dieser Befehl zeigt die queue and, welche gerade läuft.';

  aliases: string[] = [];

  category = CommandCategory.Voice;

  execute(msg: Message<boolean>, parameters: Parameters): void | Promise<void> {
    if (!msg.member?.voice.channel) {
      //TODO: No channel
      return;
    }

    if (!msg.guild) {
      return;
    }

    const guild = msg.guild;
    const queue = this.musicHandler.getQueue(guild);

    if (queue.length === 0) {
      msg.reply('Keine Songs in der Queue!');
      return;
    }

    const displaySize = 10;

    const calculatePagesCount = (pageSize: number, totalCount: number) => {
      // we suppose that if we have 0 items we want 1 empty page
      return totalCount < pageSize ? 1 : Math.ceil(totalCount / pageSize);
    };

    const pages = calculatePagesCount(displaySize, queue.length);

    const makeDisplayEmbed = (page: number): MessageEmbed => {
      const embed = new MessageEmbed();
      let amount = displaySize;
      const offset = page * displaySize;

      if (page === pages) {
        amount = queue.length - page * displaySize;
      } else if (page > pages) {
        embed.addField('--Queue--', 'Queue ist zuende');
        return embed;
      }

      if (amount === 0) {
        embed.addField('--Queue--', 'Queue ist zuende');
        return embed;
      }

      for (let i = 0; i < amount; i++) {
        const track = queue.getTrackAt(i + offset);
        if (track === undefined) {
          break;
        }

        embed.addField(`[${i + offset} | ${track.title}]`, `Länge: ${durationToString(track.duration)}\nIn ca. ${durationToString(queue.calulateLengthUntilTrack(i + offset))}\nHinzugefügt von ${'<@' + track.requestedBy?.id + '>'}`);
      }

      embed.setFooter(`Seite ${page + 1}/${pages}`);
      embed.setColor('GREEN');

      return embed;
    };

    msg.channel.send({ embeds: [makeDisplayEmbed(0)] }).then(async sent => {
      const emojiLast = await sent.react('⏪');
      const emojiNext = await sent.react('⏩');

      const listener = (reaction: MessageReaction | PartialMessageReaction, user: User | PartialUser) => {
        if (user.bot) {
          return;
        }
        const positions = sent.embeds[0].footer?.text.split('Seite ')[1].split('/');
        if (positions !== undefined) {
          const curPage = Number(positions[0]) - 1;
          const maxPages = Number(positions[1]);

          if (reaction === emojiNext) {
            reaction.users.remove(user as User);
            if (curPage < maxPages - 1) {
              sent.edit({ embeds: [makeDisplayEmbed(curPage + 1)] });
            }
          }
          if (reaction === emojiLast) {
            reaction.users.remove(user as User);
            if (curPage > 0) {
              sent.edit({ embeds: [makeDisplayEmbed(curPage - 1)] });
            }
          }
        }
      };

      setTimeout(() => {
        this.client.off('messageReactionAdd', listener);
        sent.edit({ embeds: [sent.embeds[0].setColor('RED')] });
      }, 2 * 60 * 1000);

      // TODO: Remove event after 2 mins
      this.client.on('messageReactionAdd', listener);
    });
  }
}

export default DisplayQueueCommand;
