import { inviteUrl } from '@/config.json';
import { Message } from 'discord.js';
import Command, { CommandCategory, Parameters } from '@/commands/command';

class InviteCommand implements Command {
  name = 'invite';

  category: CommandCategory = CommandCategory.Utils;

  description = 'Diese Befehl gibt einen Einladungslink für den Bot aus';

  aliases: string[] = [];

  execute(msg: Message<boolean>, parameters: Parameters): void | Promise<void> {
    msg.reply(inviteUrl);
  }
}

export default InviteCommand;
