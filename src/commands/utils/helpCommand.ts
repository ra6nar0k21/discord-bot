import DiscordBot from '@/bot';
import { Message, MessageEmbed } from 'discord.js';
import { prefix } from '@/config.json';
import Command, { CommandCategory, CommandParameter, Parameters } from '@/commands/command';

class HelpCommand implements Command {
  constructor(private bot: DiscordBot) { }

  category: CommandCategory = CommandCategory.Utils;

  name = 'help';

  description = 'Dieser Befehl gibt die generelle Hilfe aus oder eine Spezifische für einen Befehl';

  aliases: string[] = [];

  parameters: CommandParameter[] = [
    {
      name: 'command',
      description: 'Mit diesem Parameter, kann man die Hilfe von einem bestimmten Befehl bekommen',
      required: false,
    },
  ];

  execute(msg: Message<boolean>, parameters: Parameters): void | Promise<void> {
    const buildParameterList = (command: Command): string => command.parameters?.map(param => {
      if (param.required) {
        return `<${param.name}>`;
      } else {
        return `[${param.name}]`;
      }
    }).join(' ') ?? '';

    if (parameters.length === 0) {
      const messageEmbeds: Map<string, MessageEmbed> = new Map();

      for (const category in CommandCategory) {
        if (isNaN(Number(category))) {
          const embed = new MessageEmbed();

          embed.setTitle(category);
          embed.setFooter(`${prefix}help [command]`);

          messageEmbeds.set(category, embed);
        }
      }

      for (const command of this.bot.commandHandler.Commands) {
        const embed = messageEmbeds.get(CommandCategory[command.category]);
        if (embed !== undefined) {
          embed.addField(`${prefix}${command.name} ${buildParameterList(command)}`, command.description.length === 0 ? 'Keine Beschreibung' : command.description);
        }
      }

      msg.channel.send({ embeds: Array.from(messageEmbeds.values()) });
    } else {
      const cmdArg = parameters[0].toLowerCase();
      for (const command of this.bot.commandHandler.Commands) {
        if (command.name.toLowerCase() === cmdArg || command.aliases.map(a => a.toLowerCase()).indexOf(cmdArg) !== -1) {
          const embed = new MessageEmbed();

          embed.setTitle(`${prefix}${command.name} ${buildParameterList(command)}`);
          embed.setDescription(command.description);

          if (command.aliases.length !== 0) {
            embed.addField('Aliases', [command.name, ...command.aliases].join(' | '));
          }

          if (command.parameters !== undefined) {
            for (const param of command.parameters) {
              embed.addField(`Parameter ${param.name}`, `${param.description}\nBenötigt? ${param.required ? 'Ja' : 'Nein'}`);
            }
          }

          msg.channel.send({ embeds: [embed] });
        }
      }
    }
  }
}

export default HelpCommand;
