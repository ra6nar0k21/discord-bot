import { Message } from 'discord.js';
import { prefix } from '../config.json';
import Command from './command';

class CommandHandler {
  private commands: Array<Command>;

  constructor() {
    this.commands = [];
  }

  public get Commands(): Array<Command> {
    return this.commands;
  }

  public addCommand(command: Command): void {
    this.commands.push(command);
  }

  public parse(msg: Message<boolean>, rawCommand: string): void {
    const rawCommandArr = rawCommand.split(' ');

    const rawCommandName: string = rawCommandArr.splice(0, 1)[0];
    const rawCommandNameLc = rawCommandName.toLowerCase();
    let commandFound = false;

    for (const command of this.commands) {
      const commandName = command.name.toLowerCase();
      const commandAliases = command.aliases.map(alias => alias.toLowerCase());

      if (commandName === rawCommandNameLc || commandAliases.indexOf(rawCommandNameLc) !== -1) {
        commandFound = true;
        const missingArg = '';

        if (missingArg.length === 0) {
          command.execute(msg, rawCommandArr);
        } else {
          msg.reply(`Der Befehl ${rawCommandNameLc} benötigt das Argument ${missingArg}`);
        }
      }
    }

    if (!commandFound) {
      msg.reply(`Befehl ${rawCommandNameLc} nicht gefunden! Bitte nutze ${prefix}help für Hilfe!`);
    }
  }
}

export default CommandHandler;
