import { ImageUtils } from '@/utils/avatarImage';
import { Message } from 'discord.js';
import Jimp from 'jimp';
import Command, { CommandCategory, CommandParameter } from '../command';

class GatzeStriechelnCommand implements Command {
  name = 'streicheln';
  description = 'Streichelt eine Person';
  aliases = [
    'pet',
  ];

  parameters?: CommandParameter[] = [
    {
      name: 'user',
      description: 'Der nutzer, welcher gestreichelt werden soll. Muss @Nutzer sein.',
      required: true,
    },
  ];

  category = CommandCategory.Funny;

  execute(msg: Message<boolean>, parameters: string[]): void | Promise<void> {
    if (parameters.length < 1) {
      msg.reply('Zu wenig parameter');
      return;
    }

    const user = msg.mentions.members?.first();
    if (user === null || user === undefined) {
      msg.reply('Zu wenige mentions');
      return;
    }

    Jimp.read('./assets/gatze.png').then(async img => {
      if (img === null) {
        return;
      }

      const avatar = user?.avatarURL() ?? user?.user.avatarURL();

      const file = await ImageUtils.avatarUrlToPng(avatar);
      const profile = await Jimp.read(file);

      const username = user?.nickname ?? user?.user.username;
      const imgName = username
        .replaceAll(/k/g, 'g')
        .replaceAll(/K/g, 'G')
        .replaceAll(/q/g, 'g')
        .replaceAll(/Q/g, 'G')
        .replaceAll(/(?<!S|s)c[hH]?/g, 'g')
        .replaceAll(/(?<!S|s)C[hH]?/g, 'G');

      const imgPosition = {
        x: img.getWidth() / 2 + 20,
        y: img.getHeight() / 2,
      };

      const topText = {
        text: imgName,
        alignmentX: Jimp.HORIZONTAL_ALIGN_CENTER,
        alignmentY: Jimp.VERTICAL_ALIGN_TOP,
      };

      const bottomText = {
        text: `${imgName} streicheln`,
        alignmentX: Jimp.HORIZONTAL_ALIGN_CENTER,
        alignmentY: Jimp.VERTICAL_ALIGN_BOTTOM,
      };

      const font = await Jimp.loadFont(Jimp.FONT_SANS_32_WHITE);

      img.print(font,
        0,
        0,
        topText,
        img.getWidth(),
        img.getHeight(),
      );

      img.print(font,
        0,
        0,
        bottomText,
        img.getWidth(),
        img.getHeight(),
      );

      profile.scaleToFit(150, 150);

      img.blit(profile, imgPosition.x, imgPosition.y);

      const buffer = await img.getBufferAsync(Jimp.MIME_PNG);

      msg.channel.send({
        files: [
          {
            attachment: buffer,
            name: `${imgName} streicheln.png`,
          },
        ],
      });
    });
  }
}

export default GatzeStriechelnCommand;
