import { ImageUtils } from '@/utils/avatarImage';
import { Message } from 'discord.js';
import { BitmapImage, GifCodec, GifFrame, GifUtil } from 'gifwrap';
import Jimp from 'jimp';
import Command, { CommandCategory, CommandParameter } from '../command';

class UltimatePettingCommand implements Command {
  name = 'ultimatestreicheln';
  description = 'ULTIMATIV Streichelt eine Person';
  aliases = [
    'ultimatepet',
  ];

  parameters?: CommandParameter[] = [
    {
      name: 'user',
      description: 'Der nutzer, welcher ULTIMATIV gestreichelt werden soll. Muss @Nutzer sein.',
      required: true,
    },
  ];

  category = CommandCategory.Funny;

  execute(msg: Message<boolean>, parameters: string[]): void | Promise<void> {
    if (parameters.length < 1) {
      msg.reply('Zu wenig parameter');
      return;
    }

    const user = msg.mentions.members?.first();
    if (user === null || user === undefined) {
      msg.reply('Zu wenige mentions');
      return;
    }

    GifUtil.read('./assets/ultimatePetting.gif').then(async gif => {
      const avatarUrl = user?.avatarURL() ?? user?.user.avatarURL();

      const file = await ImageUtils.avatarUrlToPng(avatarUrl);
      const avatar = await Jimp.read(file);

      const newGif = await new GifCodec().encodeGif(gif.frames.map(frame => {
        const img = GifUtil.copyAsJimp(Jimp, frame) as Jimp;
        const blankImage = new Jimp(img.getWidth(), img.getHeight(), 0);

        avatar.resize(img.getWidth() - 100, img.getHeight() - 100);
        blankImage.blit(avatar, img.getWidth() / 2 - avatar.getWidth() / 2,
          img.getHeight() / 2 - avatar.getWidth() / 3);
        blankImage.blit(img,
          img.getWidth() / 2 - img.getWidth() / 2,
          img.getHeight() / 2 - img.getHeight() / 2 - 80);

        const bitmap = new BitmapImage(blankImage.bitmap);
        GifUtil.quantizeSorokin(bitmap, 256);

        return new GifFrame(bitmap, {
          disposalMethod: frame.disposalMethod, // not documented by gifwrap but it's in the source
          delayCentisecs: frame.delayCentisecs, // you'll probably want this
        });
      }), {});

      msg.channel.send({
        files: [
          {
            attachment: newGif.buffer,
            name: `ultimatestreicheln.gif`,
          },
        ],
      });
    });
  }
}

export default UltimatePettingCommand;
